package com.privalia.kafka;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.privalia.kafka.producer.Sender;

@SpringBootApplication
public class PrivaliaKafkaExampleApplication implements CommandLineRunner {

	@Autowired
	private Sender sender;
	
	@Value("${kafka.topic.helloworld}")
	private String topic;
	
	public static void main(String[] args) {
		SpringApplication.run(PrivaliaKafkaExampleApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		sender.send(topic, "Hello Franz!");
	}
	
	
}
