package com.privalia.kafka.restcontroller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.privalia.kafka.service.KafkaService;

@Controller
@RequestMapping
public class KafkaRestController {
	
	@Autowired
	private KafkaService kafkaService;
	
	@GetMapping(value = "send/{message}")
	public ResponseEntity<String> send(@PathVariable String message) {
		kafkaService.send(message);
		return ResponseEntity.ok("OK");
	}
	
	@GetMapping(value = "getAll")
	public ResponseEntity<String> getAll() {
		List<String> messages = kafkaService.getAll();
		return ResponseEntity.ok(messages.toString());
	}

}
