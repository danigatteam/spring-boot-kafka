package com.privalia.kafka.consumer;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;

public class Receiver {

	private static final Logger LOGGER = LoggerFactory.getLogger(Receiver.class);

	public static List<String> received = new ArrayList<>();
	
	private CountDownLatch latch = new CountDownLatch(1);

	public CountDownLatch getLatch() {
		return latch;
	}

	@KafkaListener(topics = "${kafka.topic.helloworld}")
	public void receive(String payload) {
		LOGGER.info("received payload='{}'", payload);
		received.add(payload);
		latch.countDown();
	}
	
	public List<String> getAll() {
		return received;
	}
}
