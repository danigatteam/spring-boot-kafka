package com.privalia.kafka.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.privalia.kafka.consumer.Receiver;
import com.privalia.kafka.producer.Sender;

@Service
public class KafkaService {
	
	@Autowired
	private Sender sender;
	
	@Autowired
	private Receiver receiver;
	
	@Value("${kafka.topic.helloworld}")
	private String topic;
	
	public void send(String message) {
		sender.send(topic, message);
	}
	
	public List<String> getAll() {
		return receiver.getAll();
	}

}
